#include <boost/test/unit_test.hpp>

#include <calculator.h>

BOOST_AUTO_TEST_CASE( calculator_instantiate )
{
    Calculator calc;
}

BOOST_AUTO_TEST_CASE( calculator_add )
{
    Calculator calc;

    BOOST_CHECK_EQUAL(calc.add(1,2), 3);
}

