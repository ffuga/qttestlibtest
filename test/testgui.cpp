#include "testgui.h"
#include <boost/test/unit_test.hpp>
#include <ui_mainwindow.h>

#include <QTest>

void TestGui::initTestCase()
{
    mApplication = new QApplication (boost::unit_test::framework::master_test_suite().argc,
                                     boost::unit_test::framework::master_test_suite().argv);
    mMainWindow = new MainWindow();
    mMainWindow->show();
}

void TestGui::testDigitInput()
{
    testDigit(mMainWindow->ui->toolButton_0, "0");
    testDigit(mMainWindow->ui->toolButton_1, "1");
    testDigit(mMainWindow->ui->toolButton_2, "2");
    testDigit(mMainWindow->ui->toolButton_3, "3");
    testDigit(mMainWindow->ui->toolButton_4, "4");
    testDigit(mMainWindow->ui->toolButton_5, "5");
    testDigit(mMainWindow->ui->toolButton_6, "6");
    testDigit(mMainWindow->ui->toolButton_7, "7");
    testDigit(mMainWindow->ui->toolButton_8, "8");
    testDigit(mMainWindow->ui->toolButton_9, "9");
}

void TestGui::testClear()
{
    QCOMPARE(mMainWindow->ui->display->text(), QString(""));
    QTest::mouseClick(mMainWindow->ui->toolButton_1, Qt::LeftButton);
    QTest::mouseClick(mMainWindow->ui->toolButton_2, Qt::LeftButton);
    QTest::mouseClick(mMainWindow->ui->toolButton_3, Qt::LeftButton);
    QCOMPARE(mMainWindow->ui->display->text(), QString("123"));
    QTest::mouseClick(mMainWindow->ui->toolButton_CL, Qt::LeftButton);
    QCOMPARE(mMainWindow->ui->display->text(), QString(""));
    mMainWindow->ui->display->clear();
}

void TestGui::testDigit(QAbstractButton *button, QString result)
{
    QTest::mouseClick(button, Qt::LeftButton);
    QCOMPARE(mMainWindow->ui->display->text(), result);
    mMainWindow->ui->display->clear();
}


BOOST_AUTO_TEST_CASE( gui_number_input )
{
    TestGui tg;
    BOOST_CHECK(QTest::qExec(&tg) == 0);
}
