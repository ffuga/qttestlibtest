#ifndef TESTGUI_H
#define TESTGUI_H

#include <mainwindow.h>
#include <QTest>

QT_BEGIN_NAMESPACE
class QAbstractButton;
QT_END_NAMESPACE

class TestGui : public QObject {
    Q_OBJECT

    QApplication *mApplication;
    MainWindow *mMainWindow = 0;

private slots:
    void initTestCase();
    void testDigitInput();
    void testClear();

private:
    void testDigit(QAbstractButton *button, QString result);
};

#endif // TESTGUI_H
