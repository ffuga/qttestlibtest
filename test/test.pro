QT       += core gui

QT += testlib

CONFIG += testcase

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtTestLibTest
TEMPLATE = app

INCLUDEPATH += ../app

SOURCES += main.cpp\
    testcalculator.cpp \
    testgui.cpp

HEADERS  += \
    testgui.h

####

SOURCES += \
        ../app/mainwindow.cpp \
        ../app/calculator.cpp

HEADERS  += ../app/mainwindow.h \
        ../app/calculator.h

FORMS    += ../app/mainwindow.ui


###


macx: INCLUDEPATH += /usr/local/include
macx: LIBS += -L/usr/local/lib

LIBS += -lboost_unit_test_framework
