#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class TestGui;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void append(QChar ch);
private slots:
    void on_toolButton_0_clicked();
    void on_toolButton_1_clicked();
    void on_toolButton_2_clicked();
    void on_toolButton_3_clicked();
    void on_toolButton_4_clicked();
    void on_toolButton_5_clicked();
    void on_toolButton_6_clicked();
    void on_toolButton_7_clicked();
    void on_toolButton_8_clicked();
    void on_toolButton_9_clicked();

    void on_toolButton_CL_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
