#-------------------------------------------------
#
# Project created by QtCreator 2016-10-05T21:47:07
#
#-------------------------------------------------

QT       += core gui

QT += testlib

CONFIG += testcase

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtTestLibTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        calculator.cpp

HEADERS  += mainwindow.h \
        calculator.h

FORMS    += mainwindow.ui
