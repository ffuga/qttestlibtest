#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::append(QChar ch)
{
    ui->display->setText(ui->display->text().append(ch));
}

void MainWindow::on_toolButton_0_clicked()
{
    append(QChar('0'));
}

void MainWindow::on_toolButton_1_clicked()
{
    append(QChar('1'));
}

void MainWindow::on_toolButton_2_clicked()
{
    append(QChar('2'));
}

void MainWindow::on_toolButton_3_clicked()
{
    append(QChar('3'));
}

void MainWindow::on_toolButton_4_clicked()
{
    append(QChar('4'));
}

void MainWindow::on_toolButton_5_clicked()
{
    append(QChar('5'));
}

void MainWindow::on_toolButton_6_clicked()
{
    append(QChar('6'));
}

void MainWindow::on_toolButton_7_clicked()
{
    append(QChar('7'));
}

void MainWindow::on_toolButton_8_clicked()
{
    append(QChar('8'));
}

void MainWindow::on_toolButton_9_clicked()
{
    append(QChar('9'));
}


void MainWindow::on_toolButton_CL_clicked()
{
    ui->display->clear();
}
